﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2Lib
{
    public partial class Developer
    {
        static readonly Developer _developer = new Developer();

        public static Developer Instance
        {
            get { return _developer; }
        }
    }
}
