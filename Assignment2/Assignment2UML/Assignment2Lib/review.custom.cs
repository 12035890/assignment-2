﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2Lib
{
    public partial class Review
    {
        static readonly Review _review = new Review();

        public static Review Instance
        {
            get { return _review; }
        }    
    }
}
