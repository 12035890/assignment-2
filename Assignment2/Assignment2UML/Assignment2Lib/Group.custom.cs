﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2Lib
{
    public partial class Group
    {

        static readonly Group _group = new Group();

        public static Group Instance
        {
            get { return _group; }
        }

        public virtual IList<Parents> ParentList
        {
            get;
            set;
        }
        public Group()
        {
            ParentList = new List<Parents>();
            Parents = ParentList;
        }
        public override string ToString()
        {
            return base.ToString() + " " + Name;
        }
    }
}
