﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2Lib
{
    public partial class Media
    {
        static readonly Media _media = new Media();
        public static Media Instance
        {
            get { return _media; }
        }
    }
}
