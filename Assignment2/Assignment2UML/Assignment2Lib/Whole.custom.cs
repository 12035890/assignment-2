﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2Lib
{
    public partial class Whole
    {

        static readonly Whole _instance = new Whole();

        public static Whole Instance
        {
            get { return _instance; }
        }

        private Whole()
        {
            GroupList = new List<Group>();
            Groups = GroupList;
        }

        public virtual IList<Group> GroupList
        {
            get;
            set;
        }

        public override string ToString()
        {
            return base.ToString() + " " + Name;
        }

    }
}
