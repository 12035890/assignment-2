﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2Lib
{
    public partial class Parents
    {
        static readonly Parents _Parents = new Parents();

        public static Parents Instance
        {
            get { return _Parents; }
        }


         public virtual IList<Review> ReviewList
        {
            get;
            set;
        }

         public virtual IList<Developer> DeveloperList
         {
             get;
             set;
         }

         public virtual IList<Media> MediaList
         {
             get;
             set;
         }
        
        public Parents()
        {
            ReviewList = new List<Review>();
            Reviews = ReviewList;
             
            DeveloperList = new List<Developer>();
            Developers = DeveloperList;

            MediaList = new List<Media>();
            MediaMulti = MediaList;
        }

        public override string ToString()
        {
            return base.ToString() + " " + Genre;
        }
    }
}
