﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Assignment2Lib;

namespace Assignment2GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //setting up everything for entry
            Group g1 = new Group();
            Group g2 = new Group();
            Group g3 = new Group();
            Group g4 = new Group();

            Parents p1 = new Parents();
            Parents p2 = new Parents();
            Parents p3 = new Parents();

            Developer d1 = new Developer();
            Developer d2 = new Developer();
            Developer d3 = new Developer();

            Media m1 = new Media();
            Media m2 = new Media();
            Media m3 = new Media();

            Review r1 = new Review();
            Review r2 = new Review();
            Review r3 = new Review();

            ///group 1 being set up below 
            ///and values being assigned
            ///to everything.
            g1.Name = "Bettlehoog";
            p1.Genre = "Shooter";
            p1.Price = 30;

            d1.Name = "EA";
            d1.Games = "20";

            m1.Image = "Angron.jpg";
            m1.Size = 579;

            r1.Author = "Fred Burger";
            r1.Score = "1/10";

            ///group 2 being set up below 
            ///and values being assigned
            ///to everything.
            g2.Name = "Cowadoodie";
            p2.Genre = "Action";
            p2.Price = 45;

            d2.Name = "Activision";
            d2.Games = "7";

            m2.Image = "Reggie.jpg";
            m2.Size = 424;

            r2.Author = "Scofeld Sollerson";
            r2.Score = "11/10";

            ///group 3 being set up below 
            ///and values being assigned
            ///to everything.
            g3.Name = "First Reality";
            p3.Genre = "Rpg";
            p3.Price = 93;

            d3.Name = "Squenix";
            d3.Games = "19530";

            m3.Image = "Jeremy.jpg";
            m3.Size = 424;

            r3.Author = "Jimmy Russels";
            r3.Score = "1/10";

            /// Access Whole.Instance Group.Instance and Parents.Instance
            Whole everything = Whole.Instance;
            everything.Name = "Filer Gaming";

            Group currentGroup = Group.Instance;

            Parents currentParent = Parents.Instance;

            ///3.Add an associated Parents / CHILD object to one GROUP
            ///below i am adding firstly the Parents to the group
            ///then im assigning each child to its Parentslist
            g1.ParentList.Add(p1);
            g2.ParentList.Add(p2);
            g3.ParentList.Add(p3);

            p1.ReviewList.Add(r1);
            p2.ReviewList.Add(r2);
            p3.ReviewList.Add(r3);

            p1.DeveloperList.Add(d1);
            p2.DeveloperList.Add(d2);
            p3.DeveloperList.Add(d3);

            p1.MediaList.Add(m1);
            p2.MediaList.Add(m2);
            p3.MediaList.Add(m3);

            ///2.Add a new group
            everything.GroupList.Add(g1);
            everything.GroupList.Add(g2);
            everything.GroupList.Add(g3);
            //add new parent
            currentGroup.ParentList.Add(p1);
            currentGroup.ParentList.Add(p2);
            currentGroup.ParentList.Add(p3);
            //add new media
            currentParent.MediaList.Add(m1);
            currentParent.MediaList.Add(m2);
            currentParent.MediaList.Add(m3);
            //add new review
            currentParent.ReviewList.Add(r1);
            currentParent.ReviewList.Add(r2);
            currentParent.ReviewList.Add(r3);
            //add new developer
            currentParent.DeveloperList.Add(d1);
            currentParent.DeveloperList.Add(d2);
            currentParent.DeveloperList.Add(d3);

            // here i am assigning the data sources their values
            wholeBindingSource.DataSource = everything;
            groupBindingSource.DataSource = everything.GroupList;
            ParentsBindingSource.DataSource = currentGroup.ParentList;
            developerBindingSource.DataSource = currentParent.DeveloperList;
            mediaBindingSource.DataSource = currentParent.MediaList;
            reviewBindingSource.DataSource = currentParent.ReviewList;

            //bitbucket repository for 12035890 Michael Filer (Me)
            //https://bitbucket.org/12035890/assignment-2
        }       
    }
}
