﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2Lib;

namespace Assignment2Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //setting up everything for entry
            Group g1 = new Group();
            Group g2 = new Group();
            Group g3 = new Group();
            
            Parents p1 = new Parents();
            Parents p2 = new Parents();
            Parents p3 = new Parents();
            
            Developer d1 = new Developer();
            Developer d2 = new Developer();
            Developer d3 = new Developer();
            
            Media m1 = new Media();
            Media m2 = new Media();
            Media m3 = new Media();

            Review r1 = new Review();
            Review r2 = new Review();
            Review r3 = new Review();

            ///group 1 being set up below 
            ///and values being assigned
            ///to everything.
            g1.Name = "Bettlehoog";
            p1.Genre = "Shooter";
            p1.Price = 30;
            
            d1.Name = "EA";
            d1.Games = "20";
            
            m1.Image = "Boop.jpg";
            m1.Size = 579;
            
            r1.Author = "Fred Burger";
            r1.Score = "1/10";

            ///group 2 being set up below 
            ///and values being assigned
            ///to everything.
            g2.Name = "Cowadoodie";
            p2.Genre = "Action";
            p2.Price = 45;
            
            d2.Name = "Activision";
            d2.Games = "7";
            
            m2.Image = "Reggie.jpg";
            m2.Size = 424;
            
            r2.Author = "Scofeld Sollerson";
            r2.Score = "11/10";

            ///group 3 being set up below 
            ///and values being assigned
            ///to everything.
            g3.Name = "First Reality";
            p3.Genre = "Rpg";
            p3.Price = 93;
            
            d3.Name = "Squenix";
            d3.Games = "19530";
            
            m3.Image = "Angron.jpg";
            m3.Size = 424;
            
            r3.Author = "Jimmy Russels";
            r3.Score = "1/10";

            /// Access Whole.Instance to get the singleton object.
            Whole everything = Whole.Instance;
            everything.Name = "Filer Gaming";
            
            ///3.Add an associated Parents / CHILD object to one GROUP
            ///below i am adding firstly the Parents to the group
            ///then im assigning each child to its Parentslist
            g1.ParentList.Add(p1);              
            g2.ParentList.Add(p2);
            g3.ParentList.Add(p3);
            
            p1.ReviewList.Add(r1);
            p2.ReviewList.Add(r2);
            p3.ReviewList.Add(r3);

            p1.DeveloperList.Add(d1);
            p2.DeveloperList.Add(d2);
            p3.DeveloperList.Add(d3);

            p1.MediaList.Add(m1);
            p2.MediaList.Add(m2);
            p3.MediaList.Add(m3);

            ///2.Add a new GROUP
            everything.GroupList.Add(g1);
            everything.GroupList.Add(g2);
            everything.GroupList.Add(g3);

            ///1.Display your WHOLE thing and all objects of the associated classes
            /// Display a list of objects of the associated classes to the console 
            /// window of the Visual Studio environment in order.    
            /// as can be seen below this foreach loop cycles through
            /// the whole, then the group, then the Parents, and all the children
            /// each time printing.
            /// below is my code printing out the company name each game in the company and 
            /// then finally the 
            System.Console.WriteLine(everything);
            foreach (Group g in everything.Groups)
            {
                System.Console.WriteLine("+++++++++++++++++++++++++++++++++++++++\n" + g + "\n+++++++++++++++++++++++++++++++++++++++\n");
                foreach (Parents p in g.Parents)
                {
                    System.Console.WriteLine("Genre : " + p.Genre);
                    System.Console.WriteLine("Price : £" + p.Price);

                    foreach (Developer d in p.Developers)
                    {
                        System.Console.WriteLine("Developer Name: " + d.Name);
                        System.Console.WriteLine("Games Made: " + d.Games);
                    }

                    foreach (Review r in p.Reviews)
                    {
                        System.Console.WriteLine("Review Author: " + r.Author);
                        System.Console.WriteLine("Game Score: " + r.Score);
                    }

                    foreach (Media m in p.MediaMulti)
                    {
                        System.Console.WriteLine("Image Location: " + m.Image);
                        System.Console.WriteLine("Image Size: " + m.Size + "\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    }
                }
            }
            ///8.Find GROUP an associated Parents/ CHILD object belongs to
            System.Console.WriteLine("Printing Parent and the associatied group");
            System.Console.WriteLine("*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/");
            int x = 1;
            foreach (Group g in everything.Groups)
            {
                foreach (Parents p in g.Parents)
                {
                    
                    System.Console.WriteLine("Parent : " + p);

                    System.Console.WriteLine("Group : " + g);
                    x++;

                }
            }
            System.Console.WriteLine("*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/\n");
            ///4.Move an associated Parents / CHILD object from one GROUP to another GROUP
            /// here i moved the Parents with all children to group 1 from group 2 
            ///i then removed Parents 1 from group 1 and Parents 2 from group 2
            ///leaving group 2 empty and swapping out group 1's Parents
            ///5.Remove an associated Parents / CHILD object from one GROUP
            ///completed with 4.
            g1.ParentList.Add(p2);
            g1.ParentList.Remove(p1);
            g2.ParentList.Remove(p2);
            
            System.Console.WriteLine(everything);

            foreach (Group g in everything.Groups)
            {
                System.Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n" + "Moving Parents!!!!!!\n" + g + "\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                ///here is some code that im using to turn the g1 Parentslist count from a bool to an int
                ///which i then check to see a groups Parents are empty
                int listCounting = g1.ParentList.Count;
                if (g.ParentList.Count == 0)
                {
                    System.Console.WriteLine(" Parent " + x + " is empty \n"); 
                }
                foreach (Parents p in g.Parents)
                {
                    System.Console.WriteLine("Genre : " + p.Genre);
                    System.Console.WriteLine("Price : £" + p.Price);

                    foreach (Developer d in p.Developers)
                    {
                        System.Console.WriteLine("Developer Name: " + d.Name);
                        System.Console.WriteLine("Games Made: " + d.Games);
                    }

                    foreach (Review r in p.Reviews)
                    {
                        System.Console.WriteLine("Review Author: " + r.Author);
                        System.Console.WriteLine("Game Score: " + r.Score);
                    }

                    foreach (Media m in p.MediaMulti)
                    {
                        System.Console.WriteLine("Image Location: " + m.Image);
                        System.Console.WriteLine("Image Size: " + m.Size + "\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    }
                }
            }
            ///6.Remove a GROUP
            everything.GroupList.Remove(g1);
            //group 1 is removed and then i print just so that you can see this has infact happend
            System.Console.WriteLine("\n Deleting a group\n");
            foreach (Group g in everything.Groups)
            {
                System.Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n" + g + "\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
            }
            ///7.List associated Parents/ CHILD objects for one GROUP
            System.Console.WriteLine("listing Parents associated with Group 1(Bettlehoog)\n");
            System.Console.WriteLine("Genre : " + p1.Genre);
            System.Console.WriteLine("Price : £" + p1.Price);
            System.Console.WriteLine("Developer Name: " + d1.Name);
            System.Console.WriteLine("Games Made: " + d1.Games);
            System.Console.WriteLine("Review Author: " + r1.Author);
            System.Console.WriteLine("Game Score: " + r1.Score);
            System.Console.WriteLine("Image Location: " + m1.Image);
            System.Console.WriteLine("Image Size: " + m1.Size + "\n");
            ///9.List GROUP(s) with no associated Parents/ CHILD object(s)            
            ///Display a list of PARTs that are empty(have no associated object(s)) or 
            /// the message “ All GROUPs have objects ”.	
            System.Console.WriteLine("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n" + "Checking groups to see if they are empty " + "\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
            foreach (Group g in everything.Groups)
            {
                int listCounting = g1.ParentList.Count;
                if (g.ParentList.Count == 0)
                {
                    System.Console.WriteLine(" Parent " + g + " Contains no Parents/children");
                }
            }
            ///10.Add your own additional functions:
            ///Test functions may be added as you feel fit – for example listing all GROUP(s) and 
            /// / or associated Parents / CHILD objects with a common attribute value, or totaling 
            /// up the value(£ or number) of some attribute etc…
            /// 
            /// Totaling the amount of Parents in the assignment
            int total = 0;
            foreach (Group g in everything.Groups)
            {
                int listCounting = g1.ParentList.Count;
                total = total + listCounting;
            }
            System.Console.WriteLine("\n There is a total of " + total + " Parents in the Groups");
            System.Console.Read();
            //bitbucket repository for 12035890 Michael Filer (Me)
            //https://bitbucket.org/12035890/assignment-2
        }
    }
}
